const path = require("path");
// const CleanWebpackPlugin = require('clean-webpack-plugin');
// const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	// entry: {
	// 	main: './src/index.js',
	// 	live: './src/live.js',
	// },
	output: {
	    path: path.join(__dirname, "/dist"),
	    filename: "[name].js",
	    chunkFilename: '[name].chunk.js',
	    // path: commonPaths.outputPath,
	    publicPath: "/"  // Notice this line
	},
	module: {
		rules: [
	      {
	        test: /\.js$/,
	        include: path.resolve(__dirname, 'src'),
	        exclude: /node_modules/,
	        use: {
	          loader: "babel-loader",
	          options: {
	            presets: ["env", "react", "stage-1"],
	            plugins: ["transform-decorators-legacy", "transform-class-properties"]
	          }
	        }
	      },
	      {
	        test: /\.(css|scss|sass)$/,
	        // Loaders are applied from right to left!
	        use: ["style-loader", "css-loader", "sass-loader"],
	      },
	      {
	        test: /\.(png|jp(e*)g|svg|gif)$/,
	        use: [
	          {
	            loader: "url-loader",
	            options: {
	              limit: 8000, // Convert images < 8kb to base64 strings
	              name: "images/[hash]-[name].[ext]"
	            }
	          }
	        ]
	      }
	    ]
    }
};