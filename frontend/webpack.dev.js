const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
	mode: 'development',
	devtool: 'inline-source-map',
	devServer: {
	    historyApiFallback: true,
	    contentBase: "./public",
	    hot: true,
	},
	plugins: [
		new HtmlWebpackPlugin({
			// filename: 'portal.html',
	  		template: path.resolve(__dirname, 'public', 'portal.html')
		})
	]
});
