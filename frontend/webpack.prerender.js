const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

// Webpack v4+ will minify code in production mode by default.

module.exports = merge(common, {
	entry: './src/prerender.js',
	mode: 'production',
	devtool: 'inline-source-map',
	devServer: {
	    historyApiFallback: true,
	    contentBase: "./",
	    hot: true
	},
	plugins: [
		new HtmlWebpackPlugin({
	  		template: path.resolve(__dirname, 'public', 'index-dev.html')
		})
	]
});
