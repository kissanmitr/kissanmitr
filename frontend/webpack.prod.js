const path = require("path");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
// const CompressionPlugin = require('compression-webpack-plugin');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');

const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;


// Webpack v4+ will minify code in production mode by default.

module.exports = merge(common, {
	mode: 'production',
	// devtool: 'source-map',
	entry: {
		main: './src/portal.js',
		// live: './src/live.js',
	},
	// optimization: {
	// 	splitChunks: {
	// 		chunks: 'all',
	// 	},
	// },
	plugins: [
		new CleanWebpackPlugin(['dist']),
		new CopyWebpackPlugin([ 
			{
				from:'public/index.html', 
				to:'index.html'
			},
			{
				from:'public/sitemap.xml', 
				to:'sitemap.xml'
			},
			{
				from:'public/robots.txt', 
				to:'robots.txt'
			},
			{
				from:'public/images', 
				to:'images/'
			} , 
			{
				from:'public/styles', 
				to:'styles/'
			}, 
			{
				from:'public/scripts', 
				to:'scripts/'
			}  
		]),
		// new CompressionPlugin({
		// 	test: /\.js(\?.*)?$/i,
		// 	deleteOriginalAssets: true,
		// }),
		new HtmlWebpackPlugin({
				filename: 'portal.html',
				template: path.resolve(__dirname, 'public', 'portal.html')
		}),
		// new BundleAnalyzerPlugin(),
	]
});
