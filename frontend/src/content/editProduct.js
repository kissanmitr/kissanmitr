const editProduct = {
  formFields: [
    {
      label: "TECHNOLOGY CATEGORY",
      name: "technologyCategory",
    },
    {
      label: "TECHNOLOGY SUB CATEGORY",
      name: "technologySubCategory",
    },
    {
      label: "COMPANY INSTITUTE NAME",
      name: "companyInstitueName",
    },
    {
      label: "PUBLIC PRIVATE",
      name: "publicPrivate",
    },
    {
      label: "TECHNOLOGY NAME",
      name: "technologyName",
    },
    {
      label: "TECHNOLOGY APPLICATION",
      name: "technologyApplication",
    },
    {
      label: "CITY STATE",
      name: "cityState",
    },
    {
      label: "COUNTRY",
      name: "country",
    },
    {
      label: "WEBSITE",
      name: "website",
    },
    {
      label: "YEAR OF ESTABLISHMENT",
      name: "yearOfEstablishment",
    },
    {
      label: "FULL ADDRESS",
      name: "fullAddress",
    },
    {
      label: "CONTACT DETAILS",
      name: "contactDetails",
    },
    {
      label: "CONTACT NAME",
      name: "contactName",
    },
    {
      label: "CONTACT DESIGNATION",
      name: "contactDesignation",
    },
    {
      label: "CONTACT EMAILID",
      name: "contactEmailId",
    },
    {
      label: "CONTACT EMAILID2",
      name: "contactEmailId2",
    },
    {
      label: "CONTACT PHONE NO",
      name: "contactPhoneNo",
    },
    {
      label: "FOUNDERS DETAILS",
      name: "foundersDetails",
    },
    {
      label: "INCUBATOR NAME",
      name: "incubatorName",
    },
    {
      label: "INCUBATOR PART OF NATIONAL INSTITUTE",
      name: "incubatorPartOfNationalInstitute",
    },
    {
      label: "PRODUCT EQUIPMENT NAME",
      name: "productEquipmentName",
    },
    {
      label: "PROBLEM STATEMENT PROCESS SHG FPO",
      name: "problemStatementProcess",
    },
    {
      label: "PROBLEM STATEMENT SOURCE",
      name: "problemStatementSource",
    },
    {
      label: "MARKET SURVEY COMPETITORS",
      name: "marketSurveyCompetitors",
    },
    {
      label: "UNIQUE FEATURES OF SOLUTION EQUIPMENT",
      name: "uniqueFeaturesOfSolutionEquipment",
    },
    {
      label: "CURRENT STAGE",
      name: "currentStage",
    },
    {
      label: "APPROVAL STAGE",
      name: "approvalStage",
    },
    {
      label: "TARGETED UNIT PRICE",
      name: "targetedUnitPrice",
    },
    {
      label: "TIME TO GO TO MARKET",
      name: "timeToGoToMarket",
    },
    {
      label: "BUSINESS MODEL",
      name: "businessModel",
    },
    {
      label: "ABOUT THE TECHNOLOGY",
      name: "aboutTheTechnology",
    },
    {
      label: "RECOGNITION",
      name: "recognition",
    },
    {
      label: "PATENT",
      name: "patent",
    },
    {
      label: "OTHER PRODUCT PORTFOLIO",
      name: "otherProductPortfolio",
    },
    {
      label: "APPLICATION AREA",
      name: "applicationArea",
    },
    {
      label: "END USER",
      name: "endUser",
    },
    {
      label: "TECHNOLOGY TRANSFER ORGANISATION",
      name: "technologyTransferOrganisation",
    },
    {
      label: "TTO NAME",
      name: "ttoName",
    },
    {
      label: "TTO DESIGNATION",
      name: "ttoDesignation",
    },
    {
      label: "TTO EMAILID",
      name: "ttoEmailId",
    },
    {
      label: "FUNDING TECH SUPPORT REQUIRED",
      name: "fundingTechSupportRequired",
    },
    {
      label: "ASSOICATED GOVERNMENT AGENCY",
      name: "associatedGovernmentAgency",
    },
    {
      label: "TRAINING SUPPORT",
      name: "trainingSupport",
    },
    {
      label: "TECHNOLOGY TRANSFER LICENSE",
      name: "technologyTransferLicense",
    },
    {
      label: "CURRENT READINESS LEVEL",
      name: "currentReadinessLevel",
    },
    {
      label: "FUNDING REQUIREMENT",
      name: "fundingRequirement",
    },
    {
      label: "PORTFOLIO",
      name: "portfolio",
    },
    {
      label: "PRODUCTION CAPACITY",
      name: "productionCapacity",
    },
    {
      label: "RAW MATERIAL",
      name: "rawMaterial",
    },
    {
      label: "VARIETIES",
      name: "varieties",
    },
    {
      label: "REGIONS",
      name: "regions",
    },
    {
      label: "SOIL",
      name: "soil",
    },
    {
      label: "PROPAGATION",
      name: "propagation",
    },
    {
      label: "AGRI PRACTICES",
      name: "agriPractices",
    },
    {
      label: "YIELD",
      name: "yield",
    },
  ],
};

export default editProduct;
