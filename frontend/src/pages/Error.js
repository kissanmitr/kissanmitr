import React, { Component } from "react";
import { Link } from 'react-router-dom';

// import LogoNearFooter from '../components/common/LogoNearFooter';

import Constant from '../scripts/constants';

// import * as remoteActions from '../scripts/remoteActions';

const kisanMitrYouTube = "https://www.youtube.com/embed/XXXXXXXXXXX?rel=0"

export default class Error extends Component {
    // componentDidMount() {
    //     remoteActions.onPageView();
    // }
	render() {
		return (
			<div className='center-align' style={{minHeight:'100%'}}>
                <div className='container'>
                	<br/>
                	<br/>
                    <Link to={Constant.Route.Main}>
                        <img src={Constant.ImageUrl.Icon.ColorFull} className="responsive-img" style={{maxWidth:200}} alt="Friends of Farmers | Kisan Mitr"/>
                    </Link>
                    <br/>
                    <br/>
                    <br/>
                    <h4><b>404</b></h4>
                    <p>Page not found</p>
                    <Link to={Constant.Route.Main}>
                        <p className="flow-text">Home</p>
                    </Link>
                    <br/>
                	{/*<YouTube url={kisanMitrYouTube} />*/}
                	<br/>
                </div>
                {/*<LogoNearFooter />*/}
            </div>
		)
	}
}

class YouTube extends Component {
    render() {
        let { url } = this.props;
        return (
            <div className="video-container">
                <iframe width="560" height="315" src={url} frameBorder="0" allow="autoplay; encrypted-media;" allowFullScreen></iframe>
            </div>
        )
    }
}
