import React from "react";
import { observer } from "mobx-react";
import { Field, Form, Formik, FormikProps } from "formik";

import Header from "../components/common/Header";
import ProductForm from "../components/shop/ProductForm";
import LogoNearFooter from "../components/common/LogoNearFooter";

// import Config from '../config';
// import Constant from '../scripts/constants';

// import * as localActions from '../scripts/localActions';
// import * as remoteActions from "../scripts/remoteActions";
// import Confetti from '../scripts/confetti';

const ProfilePage = () => {
	const addProduct = () => {
		alert("Add product");
	};
	return (
		<div>
			<Header />
			<h4>My Products</h4>
			<a className="btn-floating btn-large  waves-effect waves-light red "
				onClick={addProduct}>
				<i className="material-icons">add</i>
			</a>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<LogoNearFooter />
		</div>
	);
};

export default ProfilePage;





