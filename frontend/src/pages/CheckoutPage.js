import React, { Fragment } from "react";
import CartItems from "../components/shop/CartItems";
import Header from "../components/common/Header";

import LogoNearFooter from "../components/common/LogoNearFooter";
import AppStore from "../store/AppStore";
import { observer } from "mobx-react";

const CheckoutPage = () => {
    return (
        <div>
            <Header />
            <br />
            <div className="container">
                {AppStore.cart.length ? (
                    <Fragment>
                        <CartItems />
                        <div className="row">
                            <div className="col m3 s6">
                                <button className="btn red" onClick={() => AppStore.resetCart()}>Remove All</button>
                            </div>
                            <div className="col m3 s6">
                                <button className="btn">Checkout</button>
                            </div>
                        </div>
                    </Fragment>
                ) : (
                    <h5 className="center-align">
                        You have no items in your cart
                    </h5>
                )}
            </div>

            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <LogoNearFooter />
        </div>
    );
};

export default observer(CheckoutPage);
