import React from "react";
import ProductForm from "../components/shop/ProductForm";
import Header from "../components/common/Header";
import LogoNearFooter from "../components/common/LogoNearFooter";

const EditProduct = (props) => {
  const handleSubmit = (values) => {
    console.log(values);
  };
  return (
    <div>
      <Header />
      <br />
      <div className="container center-align">
        <h5>Add Product</h5>
        <br />
        <br />
        <ProductForm onFormSubmit={handleSubmit} initialValues={{}} />
      </div>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <LogoNearFooter />
    </div>
  );
};

export default EditProduct;
