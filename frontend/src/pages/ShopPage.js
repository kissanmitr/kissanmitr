import React, { Component } from 'react';
// import { Redirect } from 'react-router-dom';
import { observer } from 'mobx-react';

import Header from '../components/common/Header';
import FilterSidebar from '../components/common/FilterSidebar';
import ProductGrid from '../components/shop/ProductGrid';
import LogoNearFooter from "../components/common/LogoNearFooter";

// import Config from '../config';
import Constant from '../scripts/constants';

// import * as localActions from '../scripts/localActions';
// import * as remoteActions from "../scripts/remoteActions";
// import Confetti from '../scripts/confetti';

@observer
class ShopPage extends Component {

    componentDidMount() {
    //     remoteActions.fetchBanner()
    //     remoteActions.onPageView();
    //     localActions.scrollToTop();
    //     localActions.initializeMaterialize();
    //     localActions.getGradientTimer();
    //     Confetti.celebrateIfOccasion();
    }

    render() {
        // return (
        //     <Redirect to={Constant.Route.Dashboard} />
        // );
        return (
            <div>
                <Header />
                <FilterSidebar />
                <div className="row">
                    <div className="col offset-l3 l9 m12 s12">
                        <ProductGrid />
                    </div>
                </div>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
                <LogoNearFooter />
            </div>
        );
    }
}

const S = {
    whiteFullIcon: {
        width: 350,
        zIndex: 100,
    },
    shopPageCover: {
        position:'absolute', 
        top:30, 
        left:0,
        width:'100%', 
        height:400, 
        overflow:'hidden',
    },
    // shopPageContent: {
    //     position: 'relative',
    //     top: -380
    // },
}

export default ShopPage;
