import React from "react";
import { observer } from "mobx-react";
import { Field, Form, Formik, FormikProps } from "formik";

import Header from "../components/common/Header";
import ProductForm from "../components/shop/ProductForm";
import LogoNearFooter from "../components/common/LogoNearFooter";
import { Link } from "react-router-dom";
import Constants from "../scripts/constants";

// import Config from '../config';
// import Constant from '../scripts/constants';

// import * as localActions from '../scripts/localActions';
// import * as remoteActions from "../scripts/remoteActions";
// import Confetti from '../scripts/confetti';

import AppStore from "../store/AppStore";
import ProductDetails from "../components/shop/ProductDetails";

const ProductPage = (props) => {
  const { match } = props;
  return (
    <div>
      <Header />
      <br />
      {AppStore.currentUser.type === "buyer" ? (
        <ProductDetails productId={match.params.productId}/>
      ) : (
        <div className="">
          <ProductDetails productId={match.params.productId}/>
          <div className="fixed-action-btn">
            <Link
              to={Constants.Route.EditProduct(match.params.productId)}
              className="btn-floating btn-large red"
            >
              <i className="large material-icons">mode_edit</i>
            </Link>
          </div>
        </div>
      )}
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <LogoNearFooter />
    </div>
  );
};

export default ProductPage;
