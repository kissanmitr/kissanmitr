import React, { Component } from "react";
import { observer } from "mobx-react";
import AppStore from "../../store/AppStore";
import { getProducts } from "../../scripts/server";

@observer
class FilterForm extends Component {
    _toggleCategory(category) {
        let i = AppStore.filters.categories.findIndex((cat) => {
            return cat.id === category.id;
        });
        if (i >= 0) {
            AppStore.filters.categories.splice(i, 1); // Remove value at index i
        } else {
            AppStore.filters.categories.push(category);
        }
        // AppStore.setFilteredProducts()
    }
    _resetFilters() {
        AppStore.resetFilters();
        getProducts();
    }
    _toggleUnknownPriced(e) {
        AppStore.filters.includeUnknownPriced = !AppStore.filters.includeUnknownPriced
    }
    render() {
        let categories = [
            { id: "agritech", name: "Agri Tech" },
            { id: "livestocktech", name: "Livestock Tech" },
            { id: "scientific", name: "Scientific Research" },
            { id: "international", name: "International" },
        ];
        return (
            <div
                className="left-align"
                style={{
                    padding: 10,
                    paddingLeft: 15,
                    lineHeight: 2,
                }}
            >
                <button
                    className="btn btn-small"
                    onClick={() => this._resetFilters()}
                >
                    Clear Filters
                </button>
                <br/>
                <br/>
                <div>
                    <b>Category</b>
                    <br />
                    {categories.map((category, i) => {
                        return (
                            <span
                                key={i}
                                className={
                                    "chip white-text " +
                                    (AppStore.filters.categories.find(
                                        (cat) => cat.id === category.id
                                    )
                                        ? "indigo"
                                        : "blue-grey")
                                }
                                style={{ cursor: "pointer" }}
                                onClick={() => {
                                    this._toggleCategory(category);
                                }}
                            >
                                {category.name}
                            </span>
                        );
                    })}
                </div>
                <br />
                <div>
                    <b>Price ₹</b>
                    <br />
                    <div className="row">
                        <div className="col l5 m5 s5">
                            <input
                                style={S.inputBox}
                                value={AppStore.filters.price.low}
                                onChange={(e) => {
                                    AppStore.filters.price.low = e.target.value;
                                }}
                            />
                        </div>
                        <div className="col l2 m2 s2">-</div>
                        <div className="col l5 m5 s5">
                            <input
                                style={S.inputBox}
                                value={AppStore.filters.price.high}
                                onChange={(e) => {
                                    AppStore.filters.price.high =
                                        e.target.value;
                                }}
                            />
                        </div>
                    </div>
                </div>
                <div>
                    <b>Start Date - after</b>
                    <br />
                    <div className="row">
                        <div className="col">
                            <input
                                style={S.inputBox}
                                type="date"
                                value={AppStore.filters.startDate.low}
                                onChange={(e) => {
                                    AppStore.filters.startDate.low =
                                        e.target.value;
                                }}
                            />
                        </div>
                    </div>
                </div>
                <div>
                    <b>Rating above</b>
                    <br />
                    <div className="row">
                        <div className="col l5 m5 s5">
                            <input
                                style={S.inputBox}
                                value={AppStore.filters.rating.low}
                                onChange={(e) => {
                                    AppStore.filters.rating.low =
                                        e.target.value;
                                }}
                            />
                        </div>
                    </div>
                </div>
                <div>
                    <b>Other filters</b>
                    <label>
                        <input
                            type="checkbox"
                            className="filled-in"
                            checked={AppStore.filters.includeUnknownPriced}
                            onChange={e => this._toggleUnknownPriced()}
                        />
                        <span>Include products with unknown price</span>
                    </label>
                    <br />
                </div>
                <br/>
                <br/>
                <button className="btn btn-small" onClick={() => getProducts()}>
                    Apply Filters
                </button>
                <br/>
                <br/>
                <br/>
                <br/>
            </div>
        );
    }
}

const S = {
    inputBox: {
        fontSize: 12,
        height: 20,
        padding: 2,
        margin: 2,
    },
};

export default FilterForm;
