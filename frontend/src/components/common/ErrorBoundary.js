import React, { Component } from 'react'; 

export default class ErrorBoundary extends Component {
  state = {
    error: null,
    errorInfo: null
  } 
  componentDidCatch(error, errorInfo) {
    this.setState({ error, errorInfo });
  }
  render() {
    if (this.state.errorInfo) {
      return (
        <div className="container">
          <div className="row">
            <div className="col s12">
              <p className="flow-text">Error</p>
            </div>
          </div>
        </div>
      );
    }
    return this.props.children;
  }
}
