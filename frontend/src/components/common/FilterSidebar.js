import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { observer } from "mobx-react";
import M from 'materialize-css/dist/js/materialize.min.js';

import Constant from '../../scripts/constants';

// import * as localActions from "../../scripts/localActions";

import AppStore from '../../store/AppStore';
import FilterForm from "./FilterForm";

@observer
class FilterSidebar extends Component {
    componentDidMount() {
        // localActions.initializeMaterialize();
        var elems = document.querySelectorAll('.sidenav');
        var instances = M.Sidenav.init(elems, {
            preventScrolling: false,
            // closeOnClick: true,
            isOpen: true,
        });
        // var elems = document.querySelectorAll('.collapsible');
        // var instances = M.Collapsible.init(elems, {});
    }
    render() {
        return (
            <Fragment>
                <ul id="slide-out" className="sidenav sidenav-fixed blue-grey lighten-5" style={{top:62, width:250}}>
                    <div className="left-align black">
                        <Link to={Constant.Route.Main} 
                            className="btn-floating blue-grey darken-4 z-depth-2 white-text hoverable">
                            <i className="material-icons">arrow_back</i>
                        </Link>
                        <div className="user-view">
                            <p className="white-text name">Kisan Mitr</p>
                        </div>
                        <br/>
                    </div>
                    <FilterForm />
                </ul>

                <div className="fixed-action-btn" style={{bottom:20, top:'auto', left:10, right:'auto'}}>
                    <a href="#" data-target="slide-out" className={"sidenav-trigger btn-floating btn-large z-depth-4 blue-grey darken-4"}>
                        <i className="material-icons">format_list_bulleted</i>
                    </a>
                </div>

            </Fragment>
        ) 
    }
}

const S = {
    inputBox: {
        fontSize:12,
        height: 20,
        padding: 2,
        margin: 2
    }
}

export default FilterSidebar;
