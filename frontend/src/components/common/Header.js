import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { observer } from 'mobx-react';

import M from 'materialize-css/dist/js/materialize.min.js';

import Config from '../../config';
import Constant from '../../scripts/constants';

// import * as localActions from '../../scripts/localActions';

import AppStore from '../../store/AppStore';

@observer
class Header extends Component {
	componentDidMount() {
		// remoteActions.setAuthStateChangeListener();
        // localActions.initializeMaterialize();
    }
	render() {
		let { mode, backUrl, backText, title, subtitle, color, pattern, AppStore, isFreeTrial } = this.props;

		let selectedHeader = null;
		if (mode === Constant.Header.Mode.None) {
			// Must include header on all pages because header sets the authStateChange listener.
			// console.log("No Header!");
			selectedHeader = null;
		} else if (mode === Constant.Header.Mode.Payment) {
			selectedHeader = <PaymentHeader AppStore={AppStore} />;
		} else {
			selectedHeader = <MainHeader AppStore={AppStore} color={color} />;
		}
		
		return(
			<header>
				{selectedHeader}
			</header>
		)
	}
}

class MainHeader extends Component {
	componentDidMount() {
		// https://stackoverflow.com/questions/40980697/materialize-drop-downs-wont-drop-on-hover
		var elems = document.querySelectorAll('.dropdown-button');
    	var instances = M.Dropdown.init(elems, {
    		// constrainWidth: false,
    		coverTrigger: false,
    		// hover: true,
    		alignment: 'left',
    	});
	}
	render() {
		let { AppStore, color } = this.props;
		return(
			<Fragment>
				<div className="navbar-fixed">
					<nav className={"z-depth-0 "+(color? color : " indigo ")}>
						<div className="nav-wrapper">
							<ul className="left">
								<Logo />
								{/*<Placements />*/}
								<span className="hide-on-small-only">
									{/*<ViewCourses />*/}
									{/*<PagesDropdownTrigger />*/}
								</span>
							</ul>
							{/*<PagesDropdownList />*/}
							<ul className="right">
								<li>
								<Link to="/checkout" className='btn btn-floating  yellow darken-4'><i className="material-icons">shopping_cart</i></Link>
								</li>
								<Profile AppStore={AppStore} />
								<span className="hide-on-med-and-down">
									{/*<ReferAndEarn AppStore={AppStore} />*/}
								</span>
							</ul>
						</div>
					</nav>
				</div>
			</Fragment>
		)
	}
}

// ======================================================



class Logo extends Component {
	render() {
		return (
			<Fragment>
				<li>
					<a href={Constant.Route.Main} style={{height:65, paddingRight:0}}>
			    		<img 
			    			src={Constant.ImageUrl.Icon.ColorIcon} 
			    			alt="Kisan Mitr | Friends of Farmers" 
			    			className="responsive-img" 
			    			style={S.headerLogo}>
		    			</img>
			    	</a>
				</li>
				<li>
					<Link to={Constant.Route.Main}
						style={{
							fontSize: 16,
							fontWeight: 300,
							letterSpacing: 2.5,
						}}>
						<span className="left hide-on-med-and-down">
							Kisan Mitr
						</span>
					</Link>
				</li>
			</Fragment>
		);
	}
}

class BackButton extends Component {
	render() {
		let { backUrl, backText, color } = this.props;
		backUrl = backUrl || Constant.Route.Main;
		backText = backText;
		return (
			<li>
			    <Link to={backUrl} className={`btn-floating waves-effect waves-light ${color || "blue-grey"} darken-1 z-depth-0 white-text hoverable`}>
			    	<i className="material-icons">arrow_back</i>
			    </Link>
			    <span className="hide-on-small-only">{backText}</span>
			</li>
		);
	}
}

class PagesDropdownTrigger extends Component {
	render() {
		return (
			<Fragment>
			<li>
				<a className="btn-floating dropdown-button hide-on-med-and-up" data-target="pagesDropdown">
					<i className="material-icons white-text left">menu</i>
				</a>
			</li>
			<li>
				<a className="dropdown-button hide-on-small-only" data-target="pagesDropdown">
					PAGES
					<i className="material-icons white-text right">arrow_drop_down</i>
				</a>
			</li>
			</Fragment>
		)
	}
}

class PagesDropdownList extends Component {
	render() {
		return (
			<ul id="pagesDropdown" className="dropdown-content z-depth-5" style={{width:300, minWidth:300}}>
				<li tabIndex="-1"><a className="blue-grey lighten-5 black-text"><b>Pages</b></a></li>
				<li>
					<Link to={Constant.Route.Main} className="black-text">
						Home
					</Link>
				</li>
				<li className="divider" tabIndex="-1"></li>
				<li className="divider" tabIndex="-1"></li>
				<li className="divider" tabIndex="-1"></li>
			</ul>
		)
	}
}


@observer
class Profile extends Component {
	render() {
		let { AppStore } = this.props;
		return (
			<Fragment>
				{
					(true || AppStore && AppStore.auth && AppStore.auth.isLoggedIn) ?
						<li>
							<Link to={Constant.Route.Profile()} id="studyButtonBig" className="btn waves-effect waves-light blue white-text hide-on-small-only">
								Profile
								<i className="material-icons right">person</i>
							</Link>
							<Link to={Constant.Route.Profile()} className="btn-floating blue white-text z-depth-2 hoverable hide-on-med-and-up">
								<i className="material-icons">person</i>
							</Link>
						</li>
						:
						null
				}
			</Fragment>
		)
	}
}


const S = {
	headerLogo: {
		width: 46,
		marginTop: 6,
	},
}

export default Header;
