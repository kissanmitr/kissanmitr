import React, { Component, Fragment } from "react";

/*
<img class="lazy" 
	data-srcset="/img/image-to-lazy-load-2x.jpg 2x, /img/image-to-lazy-load-1x.jpg 1x" 
	data-src="/img/image-to-lazy-load-1x.jpg" 
	src="/img/placeholder.jpg" 
	alt="Alternative text to describe image.">
<noscript>
  <img 
  	srcset="/img/image-to-lazy-load-2x.jpg 2x, /img/image-to-lazy-load-1x.jpg 1x" 
  	src="/img/image-to-lazy-load-1x.jpg" 
  	alt="Alternative text to describe image.">
</noscript>
*/

class LazyImg extends Component {
	render() {
		let { dataSrc, alt, style, className } = this.props;
		return (
			<img 
				className={"lazy " + className} 
				data-src={dataSrc} 
				alt={alt || ""}
				style={style} />
		)
	}
}

class LazyIframe extends Component {
	state = {
		isLoading: true,
	}
	render() {
		let { dataSrc, style, className, width, height } = this.props;
		return (
			<Fragment>
				{
					this.state.isLoading?
						<Fragment>
							<br/>
							<br/>
	                        <div className="progress">
	                            <div className="indeterminate"></div>
	                        </div>
                        </Fragment>
                        : null
				}
				<iframe 
					width={width} 
					height={height}
					className={"lazy " + className} 
					data-src={dataSrc} 
					style={style}
					onLoad={(e)=>{
	                    this.setState({ isLoading: false });
	                }}
	            ></iframe>
            </Fragment>
		)
	}
}

export default LazyImg;
export {
	LazyImg,
	LazyIframe,
};
