import React, {Component} from 'react';
import { Link } from 'react-router-dom';
import Constant from '../../scripts/constants';

class LogoNearFooter extends Component {
    render() {
        let {color, invertLogo} = this.props;
        return (
            <div style={{minHeight:'50%', paddingTop:150, paddingBottom:150}} className={'center-align '+(color? color : 'grey lighten-4')}>
                <Link to={Constant.Route.Main}>
                    <img src={invertLogo? Constant.ImageUrl.Icon.ColorWhiteFull : Constant.ImageUrl.Icon.ColorFull} 
                        className="responsive-img" 
                        style={{maxWidth:200}} 
                        alt="Best Coding Institute | Coding Elements Logo"/>
                </Link>
                <br/>
                <br/>
                <p className='blue-grey-text text-darken-3'>
                    An initiative of the 
                    <br/>
                    Office of the Principal Scientific Adviser
                    <br/>
                    <b>Government of India</b>
                </p>
            </div>
        )
    }
}

export default LogoNearFooter;

