import React from "react";
import AppStore from "../../store/AppStore";
import { observer } from "mobx-react";

function CartItem({ product }) {
    return (
        <li className="collection-item avatar">
            <img
                src="https://via.placeholder.com/400"
                alt=""
                className="circle"
            />
            <span className="title">{product.name}</span>
            <br />
            <br />
            <div className="row">
                <div className="col l2 m3 s4">
                    <p className="yellow-text text-darken-4">₹ 299.00</p>
                </div>
                <div className="col l3 m5 s7">
                    <span
                        className="new blue-grey lighten-5 badge black-text"
                        data-badge-caption=""
                    >
                        <i
                            className="material-icons left"
                            style={{cursor: 'pointer'}}
                            onClick={() =>
                                AppStore.increaseQuantity(product.id)
                            }
                        >
                            add
                        </i>
                        <b>{product.quantity}</b>
                        <i
                            className="material-icons right"
                            style={{cursor: 'pointer'}}
                            onClick={() =>
                                AppStore.decreaseQuantity(product.id)
                            }
                        >
                            remove
                        </i>
                    </span>
                </div>
            </div>

            <a
                href="#!"
                className="secondary-content"
                onClick={() => AppStore.removeFromCart(product.id)}
            >
                <i className="material-icons red-text">remove_circle_outline</i>
            </a>
        </li>
    );
}

export default observer(CartItem);
