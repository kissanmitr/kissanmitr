import React from "react";
import { observer } from "mobx-react";

import AppStore from "../../store/AppStore";
import CartItem from "./CartItem";

function CartItems() {
    return (
        <ul className="collection">
            {AppStore.cart.map((prod) => (
                <CartItem key={prod.id} product={prod} />
            ))}
        </ul>
    );
}

export default observer(CartItems);
