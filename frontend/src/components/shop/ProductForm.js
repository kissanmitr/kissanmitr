import React, { useEffect } from "react";
import { observer } from "mobx-react";
import { Field, Form, Formik } from "formik";
import PropTypes from "prop-types";

// import Config from '../config';
// import Constant from '../scripts/constants';

// import * as localActions from '../scripts/localActions';
// import * as remoteActions from "../scripts/remoteActions";
// import Confetti from '../scripts/confetti';

const ProductForm = (props) => {
  const { onFormSubmit, initialValues } = props;
  useEffect(() => {
    var elems = document.querySelectorAll(".datepicker");
    var instances = M.Datepicker.init(elems, {
      format: "dd-mm-yyyy",
    });
  }, []);

  return (
    <div>
      <Formik
        initialValues={{ ...initialValues }}
        onSubmit={(values, actions) => {
          onFormSubmit(values);
        }}
      >
        {(formikProps) => {
          return (
            <Form>
              <div className="container">
                <div className="card">
                  <div
                    className="indigo"
                    style={{ padding: 7, marginBottom: 10 }}
                  >
                    <h6 className="white-text">Project Overview</h6>
                  </div>
                  <div className="row">
                    <div className="col m6 s12 l6">
                      <Field
                        name="projectId"
                        label="Unique Project ID"
                        disabled
                        type="text"
                        component={MyInput}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        className="datepicker"
                        name="projectStartDate"
                        label="Project Start Date"
                        component={MyInput}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        name="projectName"
                        label="Project Name"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        name="projectDescription"
                        label="Project Description"
                        component={MyTextArea}
                      />
                    </div>
                  </div>
                </div>

                <div className="card">
                  <div
                    className="indigo"
                    style={{ padding: 7, marginBottom: 10 }}
                  >
                    <h6 className="white-text">Project Technology</h6>
                  </div>
                  <div className="row">
                    <div className="col m6 s12 l6">
                      <Field
                        label="TECHNOLOGY CATEGORY"
                        name="technologyCategory"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="TECHNOLOGY SUB CATEGORY"
                        name="technologySubCategory"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="COMPANY INSTITUTE NAME"
                        name="companyInstitueName"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="PUBLIC PRIVATE"
                        name="publicPrivate"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="TECHNOLOGY NAME"
                        name="technologyName"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="TECHNOLOGY APPLICATION"
                        name="technologyApplication"
                        component={MyTextArea}
                      />
                    </div>
                  </div>
                </div>

                <div className="card">
                  <div
                    className="indigo"
                    style={{ padding: 7, marginBottom: 10 }}
                  >
                    <h6 className="white-text">Company Details</h6>
                  </div>
                  <div className="row">
                    <div className="col m6 s12 l6">
                      <Field
                        label="CITY STATE"
                        name="cityState"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="COUNTRY"
                        name="country"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="WEBSITE"
                        name="website"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="YEAR OF ESTABLISHMENT"
                        name="yearOfEstablishment"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="FULL ADDRESS"
                        name="fullAddress"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="CONTACT DETAILS"
                        name="contactDetails"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="CONTACT NAME"
                        name="contactName"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="CONTACT DESIGNATION"
                        name="contactDesignation"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="CONTACT EMAILID"
                        name="contactEmailId"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="CONTACT EMAILID2"
                        name="contactEmailId2"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="CONTACT PHONE NO."
                        name="contactPhoneNo"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="FOUNDERS DETAILS"
                        name="foundersDetails"
                        component={MyTextArea}
                      />
                    </div>
                  </div>
                </div>

                <div className="card">
                  <div
                    className="indigo"
                    style={{ padding: 7, marginBottom: 10 }}
                  >
                    <h6 className="white-text">Product Descriptions</h6>
                  </div>
                  <div className="row">
                    <div className="col m6 s12 l6">
                      <Field
                        label="INCUBATOR NAME"
                        name="incubatorName"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="INCUBATOR PART OF NATIONAL INSTITUTE"
                        name="incubatorPartOfNationalInstitute"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="PRODUCT EQUIPMENT NAME"
                        name="productEquipmentName"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="PROBLEM STATEMENT PROCESS SHG FPO"
                        name="problemStatementProcess"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="PROBLEM STATEMENT SOURCE"
                        name="problemStatementSource"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="MARKET SURVEY COMPETITORS"
                        name="marketSurveyCompetitors"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="UNIQUE FEATURES OF SOLUTION EQUIPMENT"
                        name="uniqueFeaturesOfSolutionEquipment"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="CURRENT STAGE"
                        name="currentStage"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="APPROVAL STAGE"
                        name="approvalStage"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="TARGETED UNIT PRICE"
                        name="targetedUnitPrice"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="TIME TO GO TO MARKET"
                        name="timeToGoToMarket"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="BUSINESS MODEL"
                        name="businessModel"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="ABOUT THE TECHNOLOGY"
                        name="aboutTheTechnology"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="RECOGNITION"
                        name="recognition"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="BUSINESS MODEL"
                        name="businessModel"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="PATENT"
                        name="patent"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="OTHER PRODUCT PORTFOLIO"
                        name="otherProductPortfolio"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="APPLICATION AREA"
                        name="applicationArea"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="END USER"
                        name="endUser"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="TECHNOLOGY TRANSFER ORGANISATION"
                        name="technologyTransferOrganisation"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="TTO NAME"
                        name="ttoName"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="TTO DESIGNATION"
                        name="ttoDesignation"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="TTO EMAILID"
                        name="ttoEmailId"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="FUNDING TECH SUPPORT REQUIRED"
                        name="fundingTechSupportRequired"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="ASSOICATED GOVERNMENT AGENCY"
                        name="associatedGovernmentAgency"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="TRAINING SUPPORT"
                        name="trainingSupport"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="TECHNOLOGY TRANSFER LICENSE"
                        name="technologyTransferLicense"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="CURRENT READINESS LEVEL"
                        name="currentReadinessLevel"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="FUNDING REQUIREMENT"
                        name="fundingRequirement"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="PORTFOLIO"
                        name="portfolio"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="PRODUCTION CAPACITY"
                        name="productionCapacity"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="RAW MATERIAL"
                        name="rawMaterial"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="VARIETIES"
                        name="varieties"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="REGIONS"
                        name="regions"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field label="SOIL" name="soil" component={MyTextArea} />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="PROPAGATION"
                        name="propagation"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="AGRI PRACTICES"
                        name="agriPractices"
                        component={MyTextArea}
                      />
                    </div>
                    <div className="col m6 s12 l6">
                      <Field
                        label="YIELD"
                        name="yield"
                        component={MyTextArea}
                      />
                    </div>
                  </div>
                </div>

                <div
                  className="card"
                  style={{ padding: 10, textAlign: "right" }}
                >
                  <button
                    className="waves-effect waves-light btn"
                    type="submit"
                  >
                    Publish
                  </button>
                </div>
              </div>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

const MyInput = ({ field, form, ...props }) => {
  return (
    <div className="input-field">
      <input {...field} {...props} />
      <label className="active">{props.label}</label>
    </div>
  );
};

const MyTextArea = ({ field, form, ...props }) => {
  return (
    <div className="input-field">
      <textarea
        className="materialize-textarea"
        {...field}
        {...props}
      ></textarea>
      <label className="active">{props.label}</label>
    </div>
  );
};

const propTypes = {
  onFormSubmit: PropTypes.func.isRequired,
  initialValues: PropTypes.object,
};

const defaultProps = {
  initialValues: {},
};

ProductForm.propTypes = propTypes;
ProductForm.defaultProps = defaultProps;

export default ProductForm;
