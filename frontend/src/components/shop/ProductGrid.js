import React, { Component } from 'react';
import { observer } from 'mobx-react';

import ProductCard from './ProductCard';

// import Config from '../config';
// import Constant from '../scripts/constants';

// import * as localActions from '../scripts/localActions';
// import * as remoteActions from "../scripts/remoteActions";
// import Confetti from '../scripts/confetti';

import AppStore from '../../store/AppStore';

@observer
class ProductGrid extends Component {

    render() {
        return (
            <div>
                <h5>Product Grid</h5>
                <div className="chip indigo white-text">
                    ₹{AppStore.filters.price.low} - ₹{AppStore.filters.price.high}
                </div>
                <div className="chip indigo white-text">
                    Rating above {AppStore.filters.rating.low}
                </div>
                {
                    AppStore.filters.categories.map(category => {
                        return (
                            <div className="chip indigo white-text">
                                {category.name}
                            </div>
                        )
                    })
                }
                <div className="row">
                    {
                        AppStore.products.map((prod, i) => {
                            return (
                                <div key={i} className="col l4 m6 s12">
                                    <ProductCard product={prod}/>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        );
    }
}

export default ProductGrid;
