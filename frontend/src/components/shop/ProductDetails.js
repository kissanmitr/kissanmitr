import React, { useEffect } from "react";
import M from "materialize-css/dist/js/materialize.min.js";
import AppStore from "../../store/AppStore";

const ProductDetails = ({ productId }) => {
    useEffect(() => {
        var elems = document.querySelectorAll(".materialboxed");
        var instances = M.Materialbox.init(elems, {});
    }, []);
    return (
        <div className="container center-align">
            <h5>Project Details</h5>
            <br />
            <br />
            <div className="row">
                <div className="col l5 s12">
                    <img
                        className="materialboxed responsive-img"
                        src="https://via.placeholder.com/400"
                    />
                </div>
                <div className="col l6 offset-l1 s12">
                    <h5 className="left-align">
                        Detection of presence or absence of antibody to EDS
                        virus in chicken serum.
                    </h5>
                    <div className="row">
                        <div className="col m6 s5">
                            {" "}
                            <h5 className="yellow-text text-darken-4 left-align">
                                {" "}
                                ₹ 299.00
                            </h5>
                        </div>
                        <div className="col m6 s7">
                            <a
                                className="waves-effect waves-light btn"
                                style={{ marginTop: 10 }}
                                onClick={() => AppStore.addToCart(productId)}
                            >
                                <i className="material-icons right">
                                    shopping_cart
                                </i>
                                Add to cart
                            </a>
                        </div>
                    </div>
                    <br />
                    <div className="left-align">
                        <h6>Description</h6>
                        <p>
                            Egg drop syndrome is one of the important disease
                            affecting egg production in layers caused by adeno
                            virus. EDS is prevalent in India at low levels and
                            there is an urgent need for differential diagnosis
                            from other diseases This test is based on the
                            principle “Flow through technology”. This technology
                            is an extension of the dot-enzyme linked
                            immunosorbent assay (ELISA), but is several fold
                            more user friendly and rapid
                        </p>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col l5 s12">
                    <div>
                        <ul className="collection with-header left-align">
                            <li className="collection-header">
                                <h6>Project Links</h6>
                            </li>
                            <li className="collection-item">
                                <a href="https://www.youtube.com/watch?v=i15wdPrt0wY">
                                    https://www.youtube.com/watch?v=i15wdPrt0wY
                                </a>
                            </li>
                            <li className="collection-item">
                                <a href="http://www.tanuvas.ac.in/index.html">
                                    http://www.tanuvas.ac.in/index.html
                                </a>
                            </li>
                        </ul>
                        <br />
                    </div>
                </div>
                <div className="col l6 offset-l1 s12">
                    <div className="left-align">
                        <h6>Project Info</h6>
                        <br />
                        <table className="striped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Value</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>TECHNOLOGY CATEGORY</td>
                                    <td>Livestock Tech</td>
                                </tr>
                                <tr>
                                    <td>Alan</td>
                                    <td>Jellybean</td>
                                </tr>
                                <tr>
                                    <td>Jonathan</td>
                                    <td>Lollipop</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ProductDetails;
