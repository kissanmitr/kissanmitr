import React, { Component } from 'react';
import { Link } from 'react-router-dom';
// import { observer } from 'mobx-react';

// import Config from '../config';
import Constant from '../../scripts/constants';

// import * as localActions from '../scripts/localActions';
// import * as remoteActions from "../scripts/remoteActions";
// import Confetti from '../scripts/confetti';

class ProductCard extends Component {

    render() {
        let { product } = this.props;
        return (
            <Link to={Constant.Route.Product(product.id)}>
                <div className="card small">
                    <div className="card-image">
                        <img src={"https://via.placeholder.com/400"} />
                    </div>
                    <div className="card-content">
                        <p className=''>
                        {this.props.product.name}
                        </p> 
                        <br/>
                        <div className="row">
                            <div className="col l6 m6 s6 black-text">
                            <p className="">
                            {/* ₹  */}
                            {this.props.product.startDate}
                            </p>
                            </div>
                            {/* <div className="col l6 m6 s6">
                            <a className="waves-effect waves-light btn-small"><i className="material-icons right">shopping_cart</i>Add to cart</a>
                            </div> */}
                        </div>
                    </div>
                </div>
            </Link>
        );
    }
}

export default ProductCard;
