import React, { Component } from 'react';

import AdmissionCard from './admissionCard';

class AdmissionCardList extends Component{
	render(){
		const { admissions } = this.props;
		return(
			<div>
				<div className="row">
				    {
				        admissions.map((admission, i)=>{
				            return (
				                <div key={i} className="col l6 m8 offset-m2 s12">
				                    <AdmissionCard admission={admission} />
				                </div>
				            );
				        })
				    }
				</div>
			</div>
		)
	}
}

export default AdmissionCardList;
