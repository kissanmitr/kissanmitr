import React, { Component } from 'react';

import { LazyImg } from '../common/LazyLoad';

class TeamCard extends Component {
	render() {
		const { teamMember } = this.props;
		return(
			<div>
				<br/><br/><br/><br/><br/>
				<div className="card blue-grey darken-3 z-depth-5">
					<LazyImg 
						dataSrc={teamMember.photoUrl}
                        className={"responsive-img z-depth-3 "+(teamMember.isRound?"circle":"")}
                        style={S.teamCardImg} 
                        alt={`Faculty from USA | Faculty with industry experience | Highest rated faculty | Coding Elements | ${teamMember.name}`} />
					<div className="card-content blue-grey darken-3 center-align">
						<span className="white-text">
							<h5>{teamMember.name}</h5>
							<p style={{fontSize:20,fontWeight:300}}>{teamMember.role}</p>	
							{
								teamMember.linkedinUrl?
									<div>
										<br/>
										<a className="btn-flat blue darken-2 white-text"
											target="_blank"
											href={teamMember.linkedinUrl}>
											View LinkedIn
										</a>
									</div>
									: null
							}
						</span>
					</div>
					<div className="card-content blue-grey lighten-5 center-align">
						<p className="flow-text" dangerouslySetInnerHTML={{__html: teamMember.summary}}></p>
					</div>
				</div>
			</div>
		)
	}
}

const S = {
    teamCardImg: {
		width: 150,
		marginTop: -90,
		zIndex: 100
    },
}

export default TeamCard;


