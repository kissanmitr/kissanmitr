import React, { Component } from 'react';

// import { LazyImg } from '../common/LazyLoad';
import { EnquiryButton } from '../contact';

import Constant from '../../scripts/constants';

class IntroCard extends Component {
    render() {
        return (
            <div className="card z-depth-5 center-align">
                <div className="card-content">
                    <h4>
                        Faculty from <b>USA</b>
                    </h4>
                    <p className="flow-text">
                        with experience at top companies like
                    </p>
                    <br/>
                    <br/>
                    <div className="row">
                        <div className="col l4 m4 s6 offset-s3">
                            <img src={Constant.ImageUrl.Company.LinkedIn} alt="Faculty from USA | Highest rated faculty | Coding Elements Faculty from LinkedIn" style={S.companyImg} className="responsive-img" />
                        </div>
                        <div className="col l4 m4 s6 offset-s3">
                            <img src={Constant.ImageUrl.Company.IBM} alt="Faculty from top companies | Best faculty in India | Coding Elements Faculty from IBM" style={S.companyImg} className="responsive-img" />
                        </div>
                        <div className="col l4 m4 s6 offset-s3">
                            <img src={Constant.ImageUrl.Company.Amazon} alt="Faculty with industry experience | Best faculty in Delhi NCR | Coding Elements Faculty from Amazon" style={{...S.companyImg, marginTop:20}} className="responsive-img" />
                        </div>
                    </div>
                    <h6 className="white-text">
                        (LinkedIn, California) (IBM, Bangalore) (Amazon, Noida)
                    </h6>
                </div>
                <div className="card-content blue-grey white-text">
                    <br/>
                    <p className="flow-text">
                        Register for a FREE demo class and career counseling.
                    </p>
                    <br/>
                    <EnquiryButton isLarge={true} showPulse={false} />
                    <br/>
                    <br/>
                </div>
            </div>
        )
    }
}

const S = {
    companyImg: {
        maxHeight: 40,
        margin: 10
    },
}

export default IntroCard;
