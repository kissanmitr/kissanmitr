import React, { Component } from 'react';

import { LazyImg } from '../common/LazyLoad';

import Constant from '../../scripts/constants';

class ServicesCard extends Component {
    render() {
        return (
            <div className="card z-depth-5 center-align">
                <div className="card-content">
                    <h3 style={{'fontWeight':'100'}}>
                        Want to learn coding?
                    </h3>
                    <br/>
                    <p className="flow-text">
                        Join our courses to get <b>rigorous practice</b> in software development.
                    </p>
                    <br/>
                    <p>
                        You will build and launch your own mobile app or website.
                    </p>
                    <br/>
                </div>
                <div className="card-content blue-grey darken-1 white-text">
                    <h4 style={{'fontWeight':'100'}}>
                        Need help with interviews?
                    </h4>
                    <br/>
                    <p className="flow-text">
                        Our <b>hiring experts</b> will help you get jobs at top software companies in India and abroad. 
                    </p>
                </div>
                <div className="card-content grey lighten-2">
                    <div className="row">
                        <div className="col l4 m6 s12">
                            <LazyImg dataSrc={Constant.ImageUrl.Company.Google} style={S.companyImgSmall} className="responsive-img"
                                alt="Best placements in India | Coding Elements placements at Google" />
                        </div>
                        <div className="col l4 m6 s12">
                            <LazyImg dataSrc={Constant.ImageUrl.Company.Facebook} style={S.companyImgSmall} className="responsive-img"
                                alt="Best placements in India | Coding Elements placements at Facebook" />
                        </div>
                        <div className="col l4 m6 s12">
                            <LazyImg dataSrc={Constant.ImageUrl.Company.LinkedIn} style={S.companyImgSmall} className="responsive-img"
                                alt="Best placements in India | Coding Elements placements at LinkedIn" />
                        </div>
                        <div className="col l4 m6 s12">
                            <LazyImg dataSrc={Constant.ImageUrl.Company.Amazon} style={S.companyImgSmall} className="responsive-img"
                                alt="Best placements in India | Coding Elements placements at Amazon" />
                        </div>
                        <div className="col l4 m6 s12">
                            <LazyImg dataSrc={Constant.ImageUrl.Company.Paytm} style={S.companyImgSmall} className="responsive-img"
                                alt="Best placements in India | Coding Elements placements at Paytm" />
                        </div>
                        <div className="col l4 m6 s12">
                            <LazyImg dataSrc={Constant.ImageUrl.Company.Flipkart} style={S.companyImgSmall} className="responsive-img"
                                alt="Best placements in India | Coding Elements placements at Flipkart" />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const S = {
    companyImgSmall: {
        height: 20,
        margin: 10
    },
}

export default ServicesCard;
