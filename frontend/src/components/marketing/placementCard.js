import React, { Component } from 'react';

import { LazyImg } from '../common/LazyLoad';
import { Companies } from '../../store/staticPlacements';

class PlacementCard extends Component {
	render() {
		const { placement } = this.props;
		return (
			<div className="card">
				<div className="card-image hide-on-med-and-down">
					{
						(placement.company.office)?
							<LazyImg dataSrc={placement.company.office} className="responsive-img" style={S.coverPhoto} 
								alt={`Institute with best placements | Coding Elements | Placements at top companies like ${placement.company.name}`} />
							:
							<LazyImg dataSrc={Companies.None.office} className="responsive-img" style={S.coverPhoto}
								alt={`Institute with best placements | Coding Elements | Placements at top companies like ${placement.company.name}`} />
					}
				</div>
				<div className="card-content" style={S.cardContent}>
					<div className="row">
						<div className="col l4 m5 s6">
							{
								(placement.studentPhoto)?
									<LazyImg dataSrc={placement.studentPhoto} className="circle responsive-img z-depth-1 left" style={S.profilePhoto} 
										alt={`Institute with best off-campus placements | Coding Elements | Off-campus Placements at top companies`} />
									:
									<i className="material-icons large left blue-grey-text text-lighten-4">account_circle</i>
							}
						</div>
						<div className="col offset-l4 l4 offset-m2 m5 s6">
							{
								(placement.company.logo)?
									<LazyImg dataSrc={placement.company.logo} className="responsive-img right" style={S.logo} 
										alt={`Institute with highest placement ratio | Coding Elements | Best Placements in companies like ${placement.company.name}`} />
									:
									<i className="material-icons large yellow-text text-darken-3 right">cake</i>
							}
						</div>
					</div>
					<div className="row">
						<div className="col l12 m12 s12 left-align">
							<h6 style={S.bold}>{placement.course}</h6>
							{
								placement.body ?
									<p>
										{placement.body}
									</p>
									:
									<p>
										Congratulations {placement.student} for receiving offers from {placement.company.name} and other companies.
									</p>
							}
						</div>
					</div>
				</div>
			</div>
		)
	}
}

const S = {
	cardContent: {
		height: 270,
	},
	coverPhoto: {
		height:250,
	},
    logo: {
        maxHeight: 80,
    },
    profilePhoto: {
        height: 80,
    },
    bold: {
    	fontWeight: 600,
    }
}

export default PlacementCard;
