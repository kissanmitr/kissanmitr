import React, { Component, Fragment } from 'react';

import { observer } from 'mobx-react';
import { RichText } from 'prismic-reactjs';

import { LazyImg } from '../common/LazyLoad';
import GoogleFormModal from '../common/GoogleFormModal';

import * as localActions from "../../scripts/localActions";

@observer
class EventCard extends Component {
    _isGoogleFormUrl(url) {
        if (url) {
            if (url.startsWith("https://docs.google.com") || url.startsWith("https://forms")) {
                return true;
            }
        }
        return false;
    }
	render() {
		let { event } = this.props;
		let eventData = event.data;
		let eventWhen = localActions.getDateAndTimeFromPrismicTimestamp(eventData.timestamp);
		let linkResolver = localActions.prismicLinkResolver(event);
		return (
			<div className={"card z-depth-5 left-align"}>
				<Fragment>
                    <div className="row blue-grey darken-4">
                	    <div className="col l5 m5 s12 center-align">
                	    	<br/>
                            {
                                eventData.cover_photo && eventData.cover_photo.url ?
                                    <LazyImg className="responsive-img"
                                        dataSrc={eventData.cover_photo.url} 
                                        alt={`${eventData.cover_photo.alt} | ${eventData.title[0].text} | Best coding workshops in Delhi | Coding Elements Events`} />
                                    : null
                            }
						</div>
						<div className="card-content col l7 m7 s12 white">
							{
								eventWhen.remainingNum > 0 ?
									<span className="new badge right orange" data-badge-caption="">
                                        FEATURED
                                    </span>
									: null
							}
                            {
                                eventData.title && eventData.title[0] ?
                                    <h4 className="black-text" style={{fontWeight:600}}>
                                        {eventData.title[0].text}
                                    </h4>
                                    : null
                            }
                            {
                                eventData.subtitle && eventData.subtitle[0] ?
                                    <p className="flow-text">{eventData.subtitle[0].text}</p>
                                    : null
                            }
                            <br/>
                            <div style={{fontWeight:600}}>
	                            <span style={{marginRight:20}}
	                            	className={"left text-darken-3 " + (eventWhen.remainingNum >=0? "green-text":"orange-text")}>
	                            	<i className="material-icons left">timelapse</i>
	                            	{eventWhen.remaining}
	                            </span>
                            	<span className="left" style={{marginRight:20}}>
	                            	<i className="material-icons left">date_range</i>
	                            	{eventWhen.date}
	                            </span>
                            	<span className="left">
                            		<i className="material-icons left">access_time</i>
                            		{eventWhen.time}
                            	</span>
                            </div>
                            <br/>
                            <br/>
                            <br/>
                            {
                                eventData.venue && eventData.venue[0] ?
                                    <p style={{fontWeight:600}}>
                                        <i className="material-icons left">place</i>
                                        {eventData.venue[0].text}
                                    </p>
                                    : null
                            }
                            <br/>
                            <p style={{fontWeight:600}}>
                            	<i className="material-icons left">account_balance_wallet</i>
                            	{
                            		eventData.fee?
                            			<span>Fee ₹ {eventData.fee}</span>
                            			:
                            			<span>FREE</span>
                            	}
                            </p>
                            <br/>
                            {RichText.render(eventData.description, linkResolver)}
                            <br/>
                            <br/>
                            {
                                (eventData.registration_link.url && eventWhen.remainingNum >=0) ? 
                                    ( this._isGoogleFormUrl(eventData.registration_link.url) ?
                                        <Fragment>
                                            <a href={"#"+event.id} 
                                                className="btn-large black modal-trigger">
                                                Book Your Seat
                                            </a>
                                            <GoogleFormModal 
                                                modalId={event.id} 
                                                formUrl={eventData.registration_link.url} />
                                        </Fragment>
                                        :
                                		<a href={eventData.registration_link.url} 
                                			target="_blank" 
                                			className="btn-large black">
                                			Book Your Seat
                                		</a>
                                    )
                                	:
                                	<a className="btn-flat disabled">Seats Full</a>
							}
							<br/>
						</div>
                    </div>
				</Fragment>
			</div>
		)
	}
}

export default EventCard;
