import React, { Component } from 'react';

import { LazyIframe } from '../common/LazyLoad';

import Constant from '../../scripts/constants';

class AddressCard extends Component{
    render(){
        return(
            <div className="card z-depth-5">
                <div className="card-image center-align">
                    <div className="row hide-on-large-only amber lighten-5">
                        <br/>
                        <br/>
                        <center>
                            <AddressSmallCard />
                        </center>
                        <br/>
                    </div>
                    <div className="row">
                        <div className="col l4 m12 s12" style={{padding:20}}>
                            <br/>
                            <i className="material-icons small">location_on</i>
                            <br/>
                            <a href={Constant.Contact.AddressLink} target="_blank">
                                {Constant.Contact.AddressLine1}
                                <br/>
                                {Constant.Contact.AddressLine2}
                                <br/>
                                {Constant.Contact.AddressLine3}
                            </a>
                            <br/>
                            <br/>
                            <br/>
                            <i className="material-icons">directions_railway</i>
                            <br/>
                            {Constant.Contact.MetroInfo1}
                            <h5>{Constant.Contact.MetroInfo2}</h5>
                            <br/>
                            <p>
                            {Constant.Contact.MetroInfo3}
                            </p>
                            <br/>
                        </div>
                        <div className="col l8 m12 s12 hide-on-med-and-down" style={{padding:0}}>
                            <div className="grey lighten-2">
                                <LazyIframe width={"100%"} height={550} 
                                    style={{'border':0}}
                                    dataSrc={Constant.Contact.GoogleMapEmbedLink} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

class AddressSmallCard extends Component {
    _handleClick() {
        var url = Constant.Contact.AddressLink
        var win = window.open(url, '_blank');
        win.focus();
    }
    render() {
        return (
            <div className="card-panel hoverable left-align" 
                style={{
                    maxWidth:400,
                    cursor: 'pointer'
                }} 
                onClick={this._handleClick}>
                <span className="black-text"><b>Coding Elements</b></span>
                <span className="blue-text right center-align" style={{marginLeft:10}}>
                    <i className="material-icons">directions</i>
                    <br/>
                    Directions
                </span>
                <br/>
                <span className="grey-text">
                    H-10 Express Arcade Netaji Subash Place Suite 106, Delhi, 110034
                </span>
                <br/>
                <span className="orange-text">
                    5.0
                    <i className="material-icons tiny">star</i>
                    <i className="material-icons tiny">star</i>
                    <i className="material-icons tiny">star</i>
                    <i className="material-icons tiny">star</i>
                    <i className="material-icons tiny">star</i>
                </span>
                <span className="blue-text" style={{marginLeft:10}}>
                    273 reviews
                </span>
                <br/>
                <span className="blue-text">
                    View on map
                </span>
            </div>
        )
    }
}

export default AddressCard;
export {
    AddressSmallCard
}



