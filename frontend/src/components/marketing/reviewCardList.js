import React, { Component } from 'react';

import ReviewCard from './reviewCard';

class ReviewCardList extends Component {
	render() {
		const { reviews } = this.props;
		return(
			<div>
				<div className="row">
				    {
				        reviews.map((review, i)=>{
				            return (
				                <div key={i} className="col l6 m10 offset-m1 s12">
				                    <ReviewCard review={review} />
				                </div>
				            );
				        })
				    }
				</div>
			</div>
		)
	}
}

export default ReviewCardList;
