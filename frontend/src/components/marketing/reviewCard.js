import React, { Component } from 'react';

class ReviewCard extends Component {
	render() {
		const { review } = this.props;
		return (
			<div className="card large left-align">
				<div className="card-content">
					<p className="flow-text">{review.title}</p>
					<br/>
					<div className="center-align">
						<i className="small material-icons yellow-text text-darken-2">star</i>
						<i className="small material-icons yellow-text text-darken-2">star</i>
						<i className="small material-icons yellow-text text-darken-2">star</i>
						<i className="small material-icons yellow-text text-darken-2">star</i>
						<i className="small material-icons yellow-text text-darken-2">star</i>
					</div>
					<br/>
					<div className="truncate">
						<i className="material-icons left blue-text">account_circle</i>
						<p className="grey-text">{review.author}, {review.credential}</p>
						<i>{review.course}</i>
					</div>
					<br/>
					<p dangerouslySetInnerHTML={{__html: review.body}}
						style={{height:170, overflow:'hidden'}} />
					<br/>
					<a href={review.url} className="blue-text right" target="_blank">View on {review.source}</a>
					<br/>
				</div>
			</div>
		)
	}
}

export default ReviewCard;
