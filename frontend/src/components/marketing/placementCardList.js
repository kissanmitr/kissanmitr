import React, { Component } from 'react';

import PlacementCard from './placementCard';

class PlacementCardList extends Component {
	render() {
		const { placements } = this.props;
		return (
			<div>
				<div className="row">
				    {
				        placements.map((placement, i) => {
				            return (
				                <div key={i} className="col l6 m8 offset-m2 s12">
				                    <PlacementCard placement={placement} />
				                </div>
				            );
				        })
				    }
				</div>
			</div>
		)
	}
}

export default PlacementCardList;
