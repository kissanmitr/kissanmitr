import React, { Component } from 'react';

import { ContactWidgets } from '../contact';

class ContactCard extends Component {
    render() {
        return (
            <div className="card z-depth-5">
                <div className="card-content center-align blue-grey lighten-5">
                    <div className="row" style={{marginBottom:0}}>
                        <div className="col l4 m12 s12">
                            <br/>
                            <br/>
                            <ContactWidgets.Enquiry />
                            <br/>
                            <br/>
                        </div>
                        <div className="col l4 m12 s12">
                            <br/>
                            <br/>
                            <ContactWidgets.Call />
                            <br/>
                            <br/>
                            <ContactWidgets.Email />
                            <br/>
                            <br/>
                        </div>
                        <div className="col l4 m12 s12">
                            <br/>
                            <br/>
                            <ContactWidgets.WhatsApp />
                            <br/>
                            <br/>
                            <ContactWidgets.FacebookMessenger />
                            <br/>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default ContactCard;
