import React, { Component } from 'react';
import { LazyImg } from '../common/LazyLoad';

class AdmissionCard extends Component {
	render() {
		const { admission } = this.props;
		return (
			<div className="card" style={S.card}>
				<div className="card-image">
					{
						(admission.college.campus)?
							<LazyImg dataSrc={admission.college.campus} className="responsive-img" style={S.coverPhoto} 
								alt={`Institute with best admissions | Coding Elements | Placements at top companies like ${admission.college.name}`} />
							:
							<LazyImg dataSrc={Companies.None.campus} className="responsive-img" style={S.coverPhoto}
								alt={`Institute with best admissions | Coding Elements | Placements at top companies like ${admission.college.name}`} />
					}
				</div>
				<br/>
				<div className="card-content center-align" style={{paddingTop:0}}>
					<p style={{fontSize:21 ,fontWeight:600, lineHeight:1.1, marginBottom:5}}>
						{admission.college.description}
					</p>
					{
						(admission.studentPhoto)?
							<LazyImg dataSrc={admission.studentPhoto} className="circle responsive-img z-depth-1" style={S.profilePhoto} 
								alt={`Institute with best off-campus admissions | Coding Elements | Off-campus Placements at top companies`} />
							:
							<i className="material-icons large blue-grey-text text-lighten-4">account_circle</i>
					}
					<p><b>{admission.student}</b></p>
					<p>{admission.college.name}</p>
				</div>
			</div>
		)
	}
}



const S = {
	card: {
		height: 500,
	},
	coverPhoto: {
		height:250,
	},
    logo: {
        maxHeight: 80,
    },
    profilePhoto: {
        height: 80,
    },
    bold: {
    	fontWeight: 600,
    }
}

export default AdmissionCard;
