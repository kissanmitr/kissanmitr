import React, { Component } from 'react';

const defaultUrl = "https://www.codingelements.com/events"

class Banner extends Component {
	_getRelativeUrl(url) {
		if (!url.startsWith("/")) {
			if (url.startsWith("https://www.codingelements.com")) {
				url = url.replace("https://www.codingelements.com", "")
			} else if (url.startsWith("http://www.codingelements.com")) {
				url = url.replace("http://www.codingelements.com", "")
			}
		}
		return url;
	}
	render(){
		let { title, message, imageUrl, url, color } = this.props;
		color = color || "cyan"
		return (
			<a href={url ? this._getRelativeUrl(url) : defaultUrl}>
				<div className={"card-panel "+color+" lighten-4 black-text"} style={{margin:0, padding:0}}>
					<div className="row" style={{margin:0, padding:0}}>
						<div className="col offset-l2 l2 offset-m1 m3 s12 center-align" style={{padding:10}}>
							<img className="responsive-img" 
								style={{height: 100}}
								src={imageUrl} />
						</div>
						<div className="col l6 m7 s12 left-align" style={{margin:0, padding:10}}>
							<h5>{title}</h5>
							{message}
							<br/>
						</div>
					</div>
				</div>	
			</a>
		)
	}
}


export default Banner;
