const SubdomainUrl = {
  Main: "https://www.codingelements.com",
  Online: "https://online.codingelements.com",
};

const Constant = {
  Route: {
    Main: "/",
    Shop: "/",
    Product: (productId = "") => "/product/" + productId,
    Profile: (userId = "") => "/profile/" + userId,
    EditProduct: (productId = "") => "/product/" + productId + "/edit",
  },
  Roles: {
    Teacher: "Teacher",
    Student: "Student",
  },
  LoginMethod: {
    Google: "google",
    Facebook: "facebook",
    Phone: "phone",
  },
  Header: {
    Mode: {
      None: "none",
      Main: "main",
      Online: "online",
      Dashboard: "dashboard",
      Live: "live",
      Classroom: "classroom",
      PracticeDashboard: "practiceDashboard",
      Practice: "practice",
      LiveStudent: "liveStudent",
      LiveTeacher: "liveTeacher",
      Payment: "payment",
      // Console: 'console'
    },
  },
  ShortForm: {
    Mode: {
      Default: "default",
      Bottom: "bottom",
    },
  },
  PracticeLayout: {
    Fullscreen: "fullscreen",
    Embedded: "embedded",
    Vertical: "vertical,",
  },
  Discount: {
    ReferralBonus: 1000,
    ReferralDiscount: 1000,
  },
  FreeTrial: {
    PlayedQuota: 1 * 60 * 60,
  },
  ImageUrl: {
    Company: {
      Amazon: "/images/company/company-amazon.png",
      Facebook: "/images/company/company-facebook.png",
      Flipkart: "/images/company/company-flipkart.png",
      Google: "/images/company/company-google.png",
      LinkedIn: "/images/company/company-linkedin.png",
      Paytm: "/images/company/company-paytm.png",
      IBM: "/images/company/company-ibm.png",
    },
    Icon: {
      ColorIcon: "/images/logos/logo-km.png",
      WhiteFull: "/images/logos/logo-km.png",
      ColorFull: "/images/logos/logo-km-text.png",
      ColorWhiteFull: "/images/logos/logo-km-text.png",
    },
    Team: {
      Mudit: "/images/team/team-mudit2.jpg",
    },
    Background: {
      Bootcamp1: "/images/other/bootcamp.jpg",
      Background1: "/images/other/bg-1.jpg",
      Background2: "/images/other/bg-2.jpg",
    },
    Goodies: {
      Bag: "/images/goodies/bag.png",
      Tee: "/images/goodies/tee.png",
      Certificate: "/images/goodies/certificate.png",
    },
    PythonOnlineVideoScreenshot:
      "/images/other/python-online-video-screenshot.png",
    Pacman: "/images/other/pacman4.png",
    ChatSupport: "/images/other/chat-support.png",
    CodeEditor2: "/images/other/code-editor-ide2.png",
    CodePlayer: "/images/other/code-player.png",
    PythonOnline: "/images/other/python-online.png",
    ReactNativeOnline: "/images/other/react-native-online.png",
    GoogleMap: "/images/other/map.png",
  },
  Hubspot: {
    // Enquiry: "https://share.hsforms.com/1l0dCAuNbQ3ylQZezhfV-3w43092",
    // EnquiryOnline: "https://share.hsforms.com/1l0dCAuNbQ3ylQZezhfV-3w43092",
    // EnquiryKids: "https://share.hsforms.com/1Hh6oO66hQzm7-Qzohx_7CQ43092",
  },
  GoogleFormUrl: {
    // Enquiry: "https://docs.google.com/forms/d/e/1FAIpQLSdMQOPZMse8J7cO5gdz66xKzGIgTcXoGdC-EnatfcsDNzVG-w/viewform?embedded=true&entry.1258715246&entry.1336606188&entry.10336843&entry.970085838&entry.1174667199&entry.625301971=Career+Counseling+(Included+for+FREE)&entry.1716313099",
    // EnquiryBlank: "https://docs.google.com/forms/d/e/1FAIpQLSdMQOPZMse8J7cO5gdz66xKzGIgTcXoGdC-EnatfcsDNzVG-w/viewform?embedded=true",
    // EnquiryOnline: "https://docs.google.com/forms/d/e/1FAIpQLSfqyaSfy6K-cXuOWBprMZybGDTHUMX_6a8aZsuQejAzPwYYKw/viewform?embedded=true",
    // JoinUs: "https://docs.google.com/forms/d/e/1FAIpQLSfsOjcDE6_F_-dvuCAyy7DokCUKtGefV7tRpLamNhvQlGB1Xg/viewform?embedded=true",
    // HireTalent: "https://docs.google.com/forms/d/e/1FAIpQLSfTv8qMyNLzkcGlSkSLWQYJHcIRZWQ50s5xnhjeGgmqwnJkqw/viewform?embedded=true",
    // CampusAmbassador: "https://docs.google.com/forms/d/e/1FAIpQLSeC9a-ChJLC9LGkQg8xQQlVhjvxe2Rzex6QAyAoxPYkejIE-w/viewform?embedded=true",
    // AlumniPlacements: "https://docs.google.com/forms/d/e/1FAIpQLSf5wD5Q1zLmJk8NfpBPcHPzwNB1qGu1QMisONqfkccZF-1BBg/viewform?embedded=true",
  },
  GoogleDocsUrl: {
    // RefundPolicy: "https://docs.google.com/document/d/e/2PACX-1vR9EMocwCRoNtxjidTTjP_o_kKBzG7i5exo4y9d6wlhkgDKe7udQwFk3ZMF9Y_4pqV1fpgjdyFiRKXQ/pub",
    // PrivacyPolicy: "https://docs.google.com/document/d/e/2PACX-1vTHgfVCclzMt8KgcZ7xeinuGXztUkruRPbN4lY4p0M61Z2q40KVwEYSCclWQ7uy7nvEJwKEj-XXW6Sj/pub",
    // TermsOfService: "https://docs.google.com/document/d/e/2PACX-1vT2RrwnwdCx4XLsfINQ5RQVwHs52hIm1sIDFSf2ewIq_EqdtRLSQRE-Eq0CBJwccYcRlwtTgptEbq9C/pub",
  },
  Social: {
    // YouTubeUrl: "https://www.youtube.com/c/CodingElements",
    // FacebookUrl: "https://www.facebook.com/codingelements",
    // LinkedInUrl: "https://www.linkedin.com/company/coding-elements",
    // InstagramUrl: "https://www.instagram.com/coding.elements",
    // TwitterUrl: "https://twitter.com/codingelements",
  },
  Contact: {
    PhoneLink1: "tel:919999643211", //'tel:01165806111', //'sms://+911165806111',
    Phone1: "9999-643211", //'011-65806111'
    PhoneLink2: "https://api.whatsapp.com/send?phone=919999643211",
    Phone2: "+91 9999-643211",

    FacebookMessengerLink: "https://m.me/codingelements",

    EmailLink: "mailto:info@codingelements.com",
    Email: "info@codingelements.com",

    // AddressLink: "https://www.google.co.in/maps/place/Coding+Elements/@28.6973587,77.2023763,17z/data=!3m1!4b1!4m5!3m4!1s0x390cfdf18d522bb5:0x80d931197a24f552!8m2!3d28.697354!4d77.204565?hl=en",
    // GoogleMapEmbedLink: "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3499.7421568801906!2d77.20237631509386!3d28.697358687894777!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cfdf18d522bb5%3A0x80d931197a24f552!2sCoding+Elements!5e0!3m2!1sen!2sin!4v1506069894711",
    // AddressLine1: 'Shop 25, Hudson Lane',
    // AddressLine2: '1st Floor',
    // AddressLine3: 'Delhi 110009',

    AddressLink: "https://goo.gl/maps/ZJ2qsyQUUdL2",
    GoogleMapEmbedLink:
      "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13999.555177025311!2d77.14092195296418!3d28.692973!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x80d931197a24f552!2sCoding+Elements!5e0!3m2!1sen!2sin!4v1520421906726",
    AddressLine1: "H-10 Express Arcade (Suite 106)",
    AddressLine2: "Netaji Subhash Place",
    AddressLine3: "Delhi 110034",

    // MetroInfo1: 'Near GTB Nagar Metro Station',
    // MetroInfo2: 'Gate No. 4',
    // MetroInfo3: 'Easily accessible from IIT, DTU, IIIT-D, NSIT, and IP University'

    MetroInfo1: "Near Metro Station",
    MetroInfo2: "Netaji Subhash Place",
    MetroInfo3:
      "Delhi metro pink line and red line. Easily accessible from DTU, IIITD, NSIT, IGDTUW, BVP, BPIT, MAIT, MSIT.",
  },
  Extras: {
    Quote1: `“Talk is cheap. Show me the code.” ― Linus Torvalds`,
  },
};

export default Constant;
