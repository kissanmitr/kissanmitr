import React, { Component } from "react";
import { observer } from "mobx-react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  withRouter,
} from "react-router-dom";
import yall from "yall-js";

import loadable from "@loadable/component";

// import style from './app.scss';

// import * as remoteActions from "./scripts/remoteActions";

import Constant from "./scripts/constants";

const fallback = (
  <div className="progress">
    <div className="indeterminate"></div>
  </div>
);

const ShopPage = loadable(() => import("./pages/ShopPage"), { fallback });
const ProductPage = loadable(() => import("./pages/ProductPage"), { fallback });
const ProfilePage = loadable(() => import("./pages/ProfilePage"), { fallback });
const Error = loadable(() => import("./pages/Error"), { fallback });
const EditProduct = loadable(() => import("./pages/EditProduct"), {
  fallback,
});
const CheckoutPage = loadable(() => import('./pages/CheckoutPage'), { fallback })

@observer
class App extends Component {
  componentDidMount() {
    // remoteActions.setAuthStateChangeListener();

    // Lazy loading images
    document.addEventListener("DOMContentLoaded", () => {
      yall({
        observeChanges: true, // For dynamically inserted elements
      });
    });
  }
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={ShopPage} />
          <Route exact path="/product/:productId" component={ProductPage} />
          <Route exact path="/product" component={ProductPage} />
          <Route exact path="/profile" component={ProfilePage} />
          <Route
            exact
            path="/product/:productId/edit"
            component={EditProduct}
          />
          <Route exact path='/checkout' component={CheckoutPage}/>

          {/* Should require phone verification */}

          <Route component={Error} />

          {/* <Route exact path={Constant.Route.PrismicPreview} component = {withRouter(PrismicPreviewPage)} /> */}
        </Switch>
      </Router>
    );
  }
}

export default App;
