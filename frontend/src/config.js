var Configs = {
	Release: {
		Env: 'Release',
		ServerBaseUrl: 'https://farmer.indiancst.com',
		Experiments: {
			EnableOnlinePayments: false,
		},
	},
	Staging: {
		Env: 'Staging',
		ServerBaseUrl: 'https://staging.indiancst.com',
		Experiments: {
			EnableOnlinePayments: false,
		},
	},
	Local: {
		Env: 'Local',
		ServerBaseUrl: 'http://localhost:8080',
		Experiments: {
			EnableOnlinePayments: false,
		},
	}
}

var Config = Configs.Release;
if (process.env.NODE_ENV === 'development') {
	Config = Configs.Local;
	console.log(Config.Env + " Environment");
} else if (window.location.hostname.endsWith("staging.indiancst.com") && !window.location.hostname.startsWith("farmer")){
	Config = Configs.Staging;
	console.log(Config.Env + " Environment");
} else {
	Config = Configs.Release;
	// Do not console.log
}

export default Config;
