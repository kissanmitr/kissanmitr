import { observable, action, computed, toJS } from 'mobx';

//configure({ enforceActions: true })

class AppStoreClass {

	@observable auth = {
		isLoggedIn: false,
		credential: null,
		currentUser: null,
		providerId: null,
	}

	@observable currentUser = { // This is the "Auth profile via Google" of a user
		id: null,
		email: null,
		name: null,
		photoUrl: null,
		type: 'buyer'
	}

	@observable filters = {
		categories: [],
		price: {
			low: 0,
			high: 9999999999
		},
		startDate: '',
		rating: {
			low: 0,
		},
		includeUnknownPriced: true
	}

	@action resetFilters() {
		this.filters = {
			categories: [],
			price: {
				low: 0,
				high: 9999999999
			},
			startDate: '',
			rating: {
				low: 0,
			},
			includeUnknownPriced: true
		}
	}

	@observable cart = [
		{
			id: "1",
			name: "Detection of presence or absence of antibody to EDS virus in chicken serum.",
			startDate: "2020-06-01",
			description: 'Egg drop syndrome is one of the important disease affecting egg production in layers caused by adeno virus. EDS is prevalent in India at low levels and there is an urgent need for differential diagnosis from other diseases  This test is based on the principle “Flow through technology”. This technology is an extension of the dot-enzyme linked immunosorbent assay (ELISA), but is several fold more user friendly and rapid',
			categories: 'Livestock Tech',
			quantity: 1
		},
		{
			id: "2",
			name: "Parentage verification in livestock",
			startDate: "2020-05-01",
			description: 'Identification of a set of molecular markers which can be amplified in six ruminant livestock. Selection of a set of markers amplifiable in all the six ruminant livestock species in a single multiplex PCR reaction. Current techniques adopted including maintenance of pedigree records etc are subject to human errors and thus cannot be relied upon. Kits available in market cost around $40 each animal',
			categories: 'Livestock Tech',
			quantity: 3
		},
	]

	@action addToCart(productId) {
		const products = toJS(this.products)
		const cart = toJS(this.cart)
		// add to cart only if the product isn't already in cart
		if(cart.find(prod => prod.id === productId) === undefined) { 
			const product = products.find(prod => prod.id === productId)
			product.quantity = 1;
			this.cart = [...cart,product]
		}
	}

	@action increaseQuantity(productId) {
		const cart = toJS(this.cart)
		for(let prod of cart) {
			if(prod.id === productId) {
				prod.quantity = prod.quantity + 1; 
			}
		}
		this.cart = cart
	}

	@action decreaseQuantity(productId) {
		const cart = toJS(this.cart)
		for(let prod of cart) {
			if(prod.id === productId) {
				if(prod.quantity === 1) {
					this.removeFromCart(productId)
					return
				} else {
					prod.quantity = prod.quantity - 1;
				}	
			}
		}
		this.cart = cart
	}

	@action removeFromCart(id) {
		this.cart = toJS(this.cart).filter(prod => prod.id !== id)
	}

	@action resetCart() {
		this.cart = []
	}

	// @observable searchText = ''

	// @action setSearchText(text) {
	// 	console.log(text)
	// 	this.searchText = text;
	// }

	// @observable filteredProducts = []

	// @action setFilteredProducts() {
	// 	const filters = toJS(this.filters)
	// 	const products = toJS(this.products)
	// 	const filterKeys = Object.keys(filters);
	// 	console.log(filterKeys)
    //     const prods = products.filter(prod => {
	// 			for(let eachKey of filterKeys) {
    //             console.log(eachKey)
			  
	// 		  if(eachKey === 'categories') {
	// 			  for(let cat of filters[eachKey]) {
	// 					console.log(cat.name," ",prod[eachKey])
	// 					if(cat.name === prod[eachKey])
	// 						return true;
	// 				}
	// 				return false
	// 			}
	// 		}
    //       });
    //     console.log(prods)
    //     this.filteredProducts = prods
	// }

	// @computed get searchedProducts() {
	// 	return this.products.filter((product) => {
	// 		const regex = new RegExp(`${this.searchText}`, "gi");
	// 		return (
	// 			product.name.match(regex) ||
	// 			product.description.match(regex)
	// 		);
	// 	})
	// }

	@action setCurrentUser(id, email, name, photoUrl){
		this.auth.isLoggedIn = true;
		this.currentUser.id = id;
		this.currentUser.email = email;
		this.currentUser.name = name;
		this.currentUser.photoUrl = photoUrl;
	}

	@action resetStore(){
		this.auth.isLoggedIn = false;
		this.currentUser = {};
	}

	// @computed get productCount() {
	// 	return this.productList.length
	// }

	@observable products = [
		{
			id: "1",
			name: "Detection of presence or absence of antibody to EDS virus in chicken serum.",
			startDate: "2020-06-01",
			description: 'Egg drop syndrome is one of the important disease affecting egg production in layers caused by adeno virus. EDS is prevalent in India at low levels and there is an urgent need for differential diagnosis from other diseases  This test is based on the principle “Flow through technology”. This technology is an extension of the dot-enzyme linked immunosorbent assay (ELISA), but is several fold more user friendly and rapid',
			categories: 'Livestock Tech'
		},
		{
			id: "2",
			name: "Parentage verification in livestock",
			startDate: "2020-05-01",
			description: 'Identification of a set of molecular markers which can be amplified in six ruminant livestock. Selection of a set of markers amplifiable in all the six ruminant livestock species in a single multiplex PCR reaction. Current techniques adopted including maintenance of pedigree records etc are subject to human errors and thus cannot be relied upon. Kits available in market cost around $40 each animal',
			categories: 'Livestock Tech'
		},
		{
			id: "3",
			name: "C-BRICK",
			startDate: "2020-06-01",
			description: 'Process for manufacture of speciality bricks (size 23 x 11 x 7.5 cm) through a portable C-brick machine.',
			categories: "Agri Tech"
		},
		{
			id: "4",
			name: "BRICK MANUFACTURE: SEMI MECHANISED",
			startDate: "2020-03-01",
			description: "Semi-mechanised brick extrusion machine.",
			categories: 'International'
		},
	]


}

const AppStore = new AppStoreClass();

// window.AppStore = AppStore;

export default AppStore;
