const merge = require('webpack-merge');
const commonDev = require('./webpack.dev.js');

module.exports = merge(commonDev, {
	entry: {
		main: './src/portal.js',
	},
});

